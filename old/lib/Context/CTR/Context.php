<?php declare(strict_types = 1);

namespace App\Context\CTR;

abstract class Context
{
    public $key;
    public $nonce;
    public $keyStream = '';
}
