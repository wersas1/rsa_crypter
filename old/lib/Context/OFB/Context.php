<?php declare(strict_types = 1);

namespace App\Context\OFB;

abstract class Context
{
    public $key;
    public $iv;
    public $keyStream = '';
}
