<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>AES Crypter</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">


		<!--Bootstrap-->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <!-- Styles -->
        <style>
			
			.console-log{
				height:100% !important;
				width:100% !important;
			}
			
            html, body {
				background-color:#f9f9f9;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                margin: 0;
            }

            .full-height {
                min-height: 100vh;
				height:100%;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 36px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    AES CRYPTER
                </div>

				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12">
					
					<div class="card">
						<div class="card-header bg-dark text-light">
							<span> Calculator</span>
						</div>
						<div class="card-body">		
							<form id="form" action="<!-- route('main') -->" method="GET" class="d-inline">
							@CSRF
								<select name="action" id="action" form="form">
								  <option name="encrypt" value="encrypt">Encrypt</option>
								  <option name="decrypt" value="decrypt">Decrypt</option>
								</select>
								<br>
							  Key:<br>
							  <input type="text" name="key" id="key" value="133457799BBCDFF1">
								<select name="keyType" id="keyType" form="form">
								  <option name="hex" value="hex">hex</option>
								  <option name="string" value="string">string</option>
								</select>
								<br />
								
								<div style="display: inline;padding-right: 15px;">16 symbols for HEX, 8 symbols for String type</div>
							  <br />
							  <br />
							  Message:<br />
							  <input type="text" name="message" id="message" value="0123456789ABCDEF">
								<select name="messageType" id="messageType" form="form">
								  <option name="hex" value="hex">hex</option>
								  <option name="string" value="string">string</option>
								</select>
							  
							  <input type="submit" value="Submit">
							</form> 
						<br />
						<div class="container-fluid d-inline text-dark">
							<div class="card" class="console-log">
							<div class="card-body" class="console-log">
							<h2 style="font-size:22px;">Console:</h2>
							<hr>
							<br />
							
							<span id="result" style="font-weight:bold;"></span>
							</div>
							</div>
						</div>
					</div>
					</div>
				</div>
            </div>
        </div>
    </body>
</html>
