<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>RSA Crypter</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">


    <!--Bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <!-- Styles -->
    <style>

        .console-log {
            height: 100% !important;
            width: 100% !important;
        }

        html, body {
            background-color: #f9f9f9;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            margin: 0;
        }

        .full-height {
            min-height: 100vh;
            height: 100%;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 36px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>


<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            RSA CRYPTER
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                @if (session('alert'))
                    <div style="width:100%;margin-left:auto;margin-right:auto;text-align:center;" id="alert"
                         name="alert" class="alert alert-dark bg-dark text-light">
                        {{ session('alert') }}
                    </div>
                @elseif (session('status'))
                    <div style="width:100%;margin-left:auto;margin-right:auto;text-align:center;" id="alert"
                         name="alert" class="alert alert-dark bg-dark text-light">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card" style="min-width:400px;">

                    <div class="card-header bg-dark text-light">
                        <span> Calculator</span>
                    </div>
                    <div class="card-body">

                        <form id="form" method="GET" class="d-inline" action="{{ route('main') }}">
                            @CSRF
                            <label class="mt-2" for="msg">MSG:</label><br/> <input type="text" name="msg"/> <br/>
                            <label class="mt-2" for="p">P:</label> <br/> <input type="text" name="p" id="p"/> <br/>
                            <label class="mt-2" for="q">Q:</label> <br/> <input type="text" name="q" id="q"/> <br/>
                            <label class="mt-2" for="e">E:</label> <br/> <input type="text" name="e" id="e"/> <br/>
                            <label class="mt-2" for="d">D:</label> <br/> <input type="text" name="d" id="d"/> <br/>
                            <br/>
                            <input style="margin-left: 60px;" value="Execute" class="mx-auto button btn btn-dark text-light" type="submit"/>
                        </form>


                        <br/>
                        <div class="container-fluid d-inline text-dark">
                            <div class="card" class="console-log">
                                <div class="card-body" class="console-log">
                                    <h2 style="font-size:22px;">Console:</h2>
                                    <hr>
                                    <br/>

                                    <span id="result" style="font-weight:bold;">
                                        @isset($encrypted) Encrypted message: {{$encrypted}}  <br/>
                                        @else Encrypted not set <br/>
                                        @endisset
                                        @isset($decrypted) Decrypted message: {{$decrypted}}  <br/>
                                        @else Decrypted not set <br/>
                                        @endisset
                                        @isset($ppp) ppp: {{$ppp}}
                                        @endisset
                                        @isset($n) n: {{$n}}
                                        @endisset

                                     </span>
                                    <hr>
                                    <span id="values" style="font-weight:lighter;">
                                        @isset($msg) Original message was: {{$msg}}  <br/>
                                        @endisset
                                        @isset($d) d: {{$d}}  <br/>
                                        @endisset
                                        @isset($q) q: {{$q}}  <br/>
                                        @endisset
                                        @isset($p) p: {{$p}}  <br/>
                                        @endisset
                                        @isset($e) e: {{$e}}  <br/>
                                        @endisset
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div style='font-size:10px; margin-left:30px;'>&copy 2019 VINCENTAS BAUBONIS</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
