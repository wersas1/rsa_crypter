<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainAction extends Controller
{

    /**
     * Show home index
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(Request $request)
    {

        $msg = $request->msg;
        $p = $request->p;
        $q = $request->q;
        $e = $request->e;
        $d = $request->d;

        $n = $p * $q;
        $ppp = ($p - 1) * ($q - 1);

        $raised = bcpow($msg, $e);
        $encrypted = bcmod($raised, $n);

        $raised = bcpow($encrypted, $d);
        $decrypted = bcmod($raised, $n);

        return view('index', [
            'encrypted' => $encrypted,
            'decrypted'=>$decrypted,
            'ppp'=>$ppp,
            'msg'=>$msg,
            'n'=>$n,
            'd'=>$d,
            'e'=>$e,
            'q'=>$q,
            'p'=>$p,
        ]);
    }

}
