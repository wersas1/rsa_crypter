<?php

namespace App\Http\Controllers;

class MainController extends Controller
{


/**
 * print params
 */
echo "msg(" . $_GET['msg'] . ') ';
echo "p(" . $_GET['p'] . ') ';
echo "q(" . $_GET['q'] . ') ';
echo "e(" . $_GET['e'] . ') ';
echo "d(" . $_GET['d'] . ')<br><br>';



/**
 * get params
 */
$msg = $_GET['msg'];
$p = $_GET['p'];
$q = $_GET['q'];
$e = $_GET['e'];
$d = $_GET['d'];
$n = $p * $q;
$ppp = ($p - 1) * ($q - 1);
/**
 * encrypt/decrypt message
 */
$encrypted = encrypt($msg, $e, $n);
echo ("<div style='color:red;'>Encrypted: " . $encrypted . '</div>');
$decrypted = decrypt($encrypted, $d, $n);
echo ("<div style='color:green;'>Decrypted: " . $decrypted . '</div><br>');
echo ("<div style='font-size:10px;'>&copy Vincentas Baubonis</div>");
echo "n: " . $n . "<br>";
// p-1 * q-1
echo "p-1 * q-1: " . $ppp . "<br>";
// p-1 * q-1


function encrypt($message, $e, $n)
{
    $raised = bcpow($message, $e);
    $mod = bcmod($raised, $n);
    return $mod;
}

function decrypt($encrypted, $d, $n)
{
    $raised = bcpow($encrypted, $d);
    $mod = bcmod($raised, $n);
    return $mod;
}


/**
 * test parameters
 * msg = 12
 * p = 5
 * q = 7
 * e = 5
 * d = 29
 */
