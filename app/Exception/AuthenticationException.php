<?php declare(strict_types = 1);

namespace App\Exception;

use App\Exception;

class AuthenticationException extends Exception {}
