<?php declare(strict_types = 1);

namespace App\Context\CBC;

abstract class Context
{
    public $key;
    public $iv;
}
